import { html, LitElement } from 'lit-element';
import style from './product-component-styles.js';
import '@polymer/paper-tooltip/paper-tooltip.js'
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icon/iron-icon.js';

class ProductComponent extends LitElement {
    static get properties() {
        return {
            objectCell: {
                type: Object,
                attribute: "object-cell"
            }
        };
    }

    static get styles() {
        return style;
    }

    constructor() {
        super();
        this.counterSlider = 0;
    }
    get setProduct() {
        let discount = (this.objectCell.discountedPrice > 0) ? "discountitem" : "";
        return discount
    }

    render() {
            return html `
                <div class="product_container">
                <slot name="bullet"> <p class="bullet_defalut ${this.setProduct}">Oferta</p></slot>
                <div class="slider_container">
                <div class="conteiner_icos"> 
                    <paper-icon-button @click="${ (e) => this._pushIdProduct(this.objectCell.id)}" class="favoritos" id="favoritos" icon="icons:favorite" ></paper-icon-button>
                    <paper-icon-button   @click="${ (e) => this._pushProduct(this.objectCell)}" class="zoom" id="zoom" icon="icons:zoom-in" ></paper-icon-button>
                </div>
                <paper-tooltip for="favoritos" position="left" animation-delay="0">Agregar a favoritos</paper-tooltip>
                <paper-tooltip for="zoom" position="left" animation-delay="0">Vista rápida</paper-tooltip>
                <img id="sliderImage" src="${this.objectCell.images[this.counterSlider]}" alt="Detalle del producto">
                ${(this.objectCell.images.length>1)?
                    html`
                    <button class="button left"  @click=${(e) => this._muveSlider(+1)} ><</button>
                    <button class="button right" @click=${(e) => this._muveSlider(-1)} >></button>`
                    : html``}
                </div>
                <div class="container_title" @click="${this.productSelected}">
                    <p class="title">${this.objectCell.title}</p>
                </div> 
                <div class="price_container ${this.setProduct}" @click=${this.productSelected}>
                    <p class="price">$${this._formatCurrency(this.objectCell.price)}</p>
                    ${this.objectCell.discountedPrice>0? 
                        html`<p class="discount">
                            $${this._formatCurrency(this.objectCell.discountedPrice)}
                            </p>`:
                        html``}
                    
                    <!--
                    <p class="shopping"><a href="#" @click="${ (e) =>  this._pushProduct(this.objectCell)}"  ><iron-icon icon="icons:shopping-cart"></iron-icon> Agregar al carrito</a></p>-->
                </div>
                    <div class="bottomContainerSlot">
                        <p class="promotionText">
                        ó hasta en 8 pagos de $80 
                        </p>
                    </div>
                </div>
            `;
    }
    _formatCurrency(number) {
        return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
    _muveSlider(number) {
        let sliderlenght = this.objectCell.images.length - 1;
        this.counterSlider += number;
        if (this.counterSlider < 0) {
            this.counterSlider = sliderlenght
        } else if (this.counterSlider > sliderlenght) {
            this.counterSlider = 0
        }
        this.myimage = this.shadowRoot.querySelector('#sliderImage')
        this.myimage.src = this.objectCell.images[this.counterSlider]
    }
    _pushIdProduct(idProduct) {
        let event = new CustomEvent('get-id-product', {
            detail: {
                id_productg: idProduct
            }
        });
        this.dispatchEvent(event);
    }
    _pushProduct(porduct) {
        let event = new CustomEvent('get-product', {
            bubbles:false,
            composed:false,
            detail: {
                id_productg: porduct
            }
        });
        this.dispatchEvent(event);
    }

    productSelected(event){
        this.dispatchEvent(new CustomEvent('product-selected',{
            bubbles:false,
            composed:false,
            detail:this.objectCell
        }));
    }
}
window.customElements.define("product-component", ProductComponent);