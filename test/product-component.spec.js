/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../product-component.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<product-component></product-component>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
