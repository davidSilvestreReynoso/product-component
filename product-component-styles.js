import { css } from 'lit-element';

export default css`
:host {
  display: inline-block;
  box-sizing: border-box;
  display: inline-block;
  box-sizing: border-box;
  background-color: #fff;
  border-radius: 5px;
  min-width: var(--product-component-min-width, 260px);
  max-width: var(--product-component-max-width, 250px);
  --paper-icon-button-ink-color: #fff;
  --paper-tooltip-background: var(--tooltip-background, #000);
  --paper-tooltip-opacity: var(--tooltip-opacity, 1);
  border: var(--padding-slider-container, 1px solid #ccc);
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2); }

:host([hidden]),
[hidden] {
  display: none !important; }

*,
*:before,
*:after {
  box-sizing: inherit;
  font-family: inherit; }

.product_container {
  position: relative;
  border-radius: 5px;
  cursor: pointer; }
  .product_container slot p.bullet_defalut {
    display: none; }
    .product_container slot p.bullet_defalut.discountitem {
      display: block;
      margin: 0;
      position: absolute;
      z-index: 1;
      background: #00305E;
      color: #fff;
      width: var(--width-bullet, 70px);
      height: var(--height-bullet, 40px);
      font-size: 13px;
      border-radius: var(--border-radius-bullet, 50px);
      padding: var(--padding-bullet, 10px);
      top: var(--position-top-bullet, 10px);
      bottom: var(--position-bottom-bullet, 0px);
      left: var(--position-left-bullet, 5px);
      right: var(--position-right-bullet, 0px);
      text-align: center;
      font-weight: 500; }
  .product_container .slider_container {
    text-align: center;
    position: relative;
    padding: var(--padding-slider-container, 10px);
    border-bottom: var(--padding-slider-container, 1px solid rgba(0, 0, 0, 0)); }
    .product_container .slider_container .conteiner_icos {
      position: absolute;
      z-index: 1;
      right: var(--right-position-icons, 10px);
      top: var(--top-position-icons, 10px);
      left: var(--left-position-icons, auto);
      bottom: var(--bottom-position-icons, auto);
      opacity: 0;
      transition: opacity 0.5s ease-in-out; }
      .product_container .slider_container .conteiner_icos paper-icon-button {
        display: block;
        padding: 0;
        width: var(--widhIcon, 25px);
        height: var(--heightIcon, 25px); }
        .product_container .slider_container .conteiner_icos paper-icon-button.favoritos {
          color: var(--colorIconFavoritos, #00305e); }
        .product_container .slider_container .conteiner_icos paper-icon-button.zoom {
          color: var(--colorIconZoom, #00305e); }
    .product_container .slider_container img {
      width: var(--height-width, 100%);
      height: var(--height-image, 250px); }
    .product_container .slider_container .button {
      top: 0;
      border-radius: 5px;
      position: absolute;
      font-size: 25px;
      height: 100%;
      border-color: #fff;
      background: #FFF;
      font-weight: 500;
      border: none;
      outline: none; }
      .product_container .slider_container .button.left {
        left: 0; }
      .product_container .slider_container .button.right {
        right: 0; }
  .product_container .container_title {
    border-bottom: var(--padding-slider-container, 1px solid rgba(0, 0, 0, 0));
    padding: 10px 0px; }
    .product_container .container_title .title {
      text-align: var(--align-title-product, center);
      font-size: var(--font-size-title-product, 15px);
      font-weight: var(--font-weight-title-product, 600);
      margin: var(--margin-title-product, 0px);
      padding: var(--padding-title-product, 0 5px);
      color: var(--color-title-product, #00305E); }
  .product_container .price_container {
    display: flex;
    flex-flow: row;
    flex-wrap: nowrap;
    justify-content: center;
    align-items: center; }
    .product_container .price_container p {
      margin: 10px 0px 0px 0px; }
    .product_container .price_container .price {
      padding: 10px 0;
      font-size: 20px;
      color: var(--sellfone-product-component-prioce-color, #00305e);
      font-weight: 500; }
    .product_container .price_container .discount {
      padding: 10px 0; }
    .product_container .price_container.discountitem {
      padding: 0 10%;
      justify-content: space-evenly; }
      .product_container .price_container.discountitem .discount {
        font-size: 20px;
        color: var(--sellfone-product-component-prioce-color, #00305e);
        font-weight: 500; }
      .product_container .price_container.discountitem .price {
        padding: 10px 0;
        font-size: 18px;
        color: #ccc;
        text-decoration: line-through; }
    .product_container .price_container .shopping {
      width: 100%;
      margin: 0px;
      margin: 15px 0px;
      transform: scale(0.8, 0.8);
      transition: transform 0.5s ease-in-out; }
      .product_container .price_container .shopping a {
        text-decoration: none;
        padding: 5px;
        color: black;
        color: var(--color-add-product, #00305E); }
  .product_container:hover .slider_container .conteiner_icos {
    opacity: 1; }
  .product_container:hover .price_container .shopping {
    bottom: 0;
    transform: scale(1, 1); }
  .product_container:hover .title {
    text-decoration: underline; }
  .product_container .bottomContainerSlot {
    text-align: center;
    width: 100%;
    background-color: #f4da40;
    border-bottom-right-radius: 5px;
    border-bottom-left-radius: 5px; }
    .product_container .bottomContainerSlot .promotionText {
      color: #00305E;
      padding: 10px 0;
      font-weight: 500;
      margin: 0; }
`;
